# MySQL Basic Notes

#### Enter MySQL

`mysql -u username -p password` or `mysql -u username -p password database_name` to access specific database directly.

#### Dump Mysql Database table data
`mysqldump -u username -p password database_name table_name > dist.sql` can dump the database table data and table structure., the argument ` -d, --no-data       No row information.` to export only including table structure.

#### Dump Mysql Database table data by where clause
` mysqldump -u username -p password -h host_name database_name table_name --where="True order by vote desc limit 10" > top_votes.sql`

`mysqldump -u root -p rouchi_dev lesson_resources  --lock-all-tables --where="lesson_id in (select id from lessons where course_id in (214,215,216,227,228))" > lesson_resources.sql`

#### Import data to database
enter database,run the command `source dist.sql`


#### Export Mysql Specific data to local file
`mysql -u username -p password -h host_name -e "SELECT * FROM database.table" > data.txt`, `-e` mean execute sql command.

#### Change Mysql default charset to UTF-8
`vim /etc/my.cnf`, add following lines:

    [client]
	default-character-set=utf8

	[mysqld]
	collation-server = utf8_general_ci
	character-set-server = utf8
	
then run `show variables like '%char%';` in mysql command to check it work.

if `collation_database` still `latin1_swedish_ci`,try run `ALTER DATABASE databasename CHARACTER SET utf8 COLLATE utf8_general_ci;`

add serial number for select query rows: `select (@rowNo := @rowNo+1) as n, * from table_name, (select @rowNo:=0) table_name`

#### Look mysql default options
`mysqld --verbose --help` or `mysqladmin variables`

#### sql_mode issue
[mysqld]
`sql_mode = ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION`


