# Java Remote Debug with Tomcat and Intellij IDEA

Debug is essiential for software development, it is possible for java web server project debug remote? Definitely yes!

#### Configure Tomcat Catalina
It'd better to set catalilna variable in `/bin/setenv.sh` instead `/bin/catalina.sh`.

Here is the `setenv.sh` configuration:

    
    CATALINA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,address=8090,server=y,suspend=n"
    
Then ,restart the Tomcat.

#### Set Intellij IDEA Debug configuration
1. Run > Edit Configurations
2. Add a configuration,select **Remote** option,configure remote **ip_address** and **port**
3. Specify the debug project source directory
4. Apply then run debug