# Logstash and JDBC plugin Notes


JDBC plugin for Logstash can import data from any Rational Database that support the JDBC interface.

#### Reference links:
- https://www.elastic.co/blog/logstash-jdbc-input-plugin
- https://www.elastic.co/guide/en/logstash/master/advanced-pipeline.html
- https://www.elastic.co/guide/en/logstash/current/plugins-inputs-jdbc.html
- https://www.elastic.co/guide/en/logstash/master/plugins-outputs-elasticsearch.html
- http://blog.csdn.net/yeyuma/article/details/50240595


### Getting Started

#### Installation
1. Install Logstash from official website,then extract the file to local directory
2. Switch **Ruby** gem resource to `https://ruby.taobao.com`,and **vim** `Gemfile` file in logstash installation directory,change source file vaule to `https://ruby.taobao.com`.
3. Install JDBC plugin for Logstash `$ bin/plugin install logstash-input-jdbc`

#### MySQL connector
Copy `mysql-connector-java-version.jar` to installation directory.

#### Configuration
**vim** `jdbc-plugin.conf` file in installation directory. Edit it:

```
input{
  jdbc {
    jdbc_driver_library => "mysql-connector-java-5.1.38.jar"
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    jdbc_connection_string => "jdbc:mysql://localhost:3306/person"
    jdbc_user => "root"
    jdbc_password => "smile"
    schedule => "* * * * *"
    statement => "SELECT * from Person"
  }
}

output{
  elasticsearch {
        index => "person"
        document_type => "person"
        document_id => "%{id}"
        hosts => "127.0.0.1"
    }
    stdout {
          codec => json_lines
      }
}

```

#### Test Configuration
Test configuration,run the following command:
`$ bin/logstash -f jdbc-plugin.conf --configtest`,
the `--configtest` option parses the configuration file and reprots any errors,
if show `Configuration OK`,congratulations it work!

#### Run it!
Run command:`$ bin/logstash -f jdbc-plugin.conf`

    
    
    
    
    
    
    
    
    


