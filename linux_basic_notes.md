# Linux Basic

### find text in files
`grep --include=\*.xml --exclude=\directory -rn . -e "text"`

### rsync - remote sync
`rsync -avz ubuntu@[ip_address]:/home/directory/file.ext ./` 